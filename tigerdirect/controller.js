import bodymen, { errorHandler } from "bodymen";
import { fetchReviewService } from './service'

export const reviewCtrl = async ({ bodymen: { body }, params }, res, next) => {
    try {
        let result = await fetchReviewService(body);
        res.status(200).json(result);
    }
    catch (err) {
        console.log("errin controller", err)
        res.status(500).json(err);
    }
}