var express = require('express');
var router = express.Router();
import { reviewCtrl } from "./controller";
import { middleware as body } from 'bodymen'

/* GET home page. */
router.post('/findReviews',
  body({
    id: {
      type: String,
      required: true
    }
  }), reviewCtrl)

module.exports = router;
