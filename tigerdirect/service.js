var cheerio = require('cheerio');
var http = require('http');
var request = require('request');



export const fetchReviewService = async params => {
    try {
        let id = params.id;
        let html = await sendRequest(id);
        let response = await findReviews(html);
        console.log(response)
        return response

    }
    catch (err) {
        console.log(err);
        let response = { error: err };
        throw response;
    }
}

const sendRequest = async id => {
    return new Promise(function (resolve, reject) {
        request({
            uri: `http://www.tigerdirect.com/applications/SearchTools/item-details.asp?EdpNo=${id}`,
            // uri: 'http://www.thenewschool.org/',
            // qs: { access_token: PAGE_ACCESS_TOKEN },
            method: 'GET',
            // json: messageData
            headers: { 'user-agent': 'node.js', 'Accept': '/*/' }

        }, function (error, response, body) {
            console.log(response.statusCode)

            if (!error && response.statusCode == 200) {
                // console.log(body)
                resolve(body);
            } else {
                console.log("error occured in sned request", error)
                reject("URL generated from id is not valid");
            }
        });
    })
}

const findReviews = async html => {
    return new Promise(function (resolve, reject) {
        try {
            let allReviews = [];
            var $ = cheerio.load(html, { decodeEntities: true, ignoreWhitespace: true });
            $('div.review').each(function (i, element) {
                let compObj = { comment: {} };
                let obj = {};
                var parent = $(this);
                let leftChild = parent.children('.leftCol');
                let review = leftChild.children('.itemReview').find('dt');
                review.each(function (i, element) {
                    var a = $(this);
                    let key = $(a)
                    obj[key.text().trim()] = a.parent().children('dd').eq(i).text().trim();
                })
                compObj.rating = obj;
                let nameDate = leftChild.children('.reviewer');
                compObj.name = nameDate.children('dd').eq(0).text().trim();
                compObj.date = nameDate.children('dd').eq(1).text().trim();
                let rightChild = parent.children('.rightCol');
                compObj.comment.title = rightChild.find('h6').text().trim();
                compObj.comment.text = rightChild.find('p').text().trim();
                // console.log(compObj);
                allReviews.push(compObj);
            });
            resolve(allReviews);
        }
        catch (err) {
            console.log(err);
            reject(err);
        }
    })
}
